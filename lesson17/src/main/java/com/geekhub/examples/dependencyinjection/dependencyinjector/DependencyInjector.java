package com.geekhub.examples.dependencyinjection.dependencyinjector;

public interface DependencyInjector {
    <T> T getBean(Class<T> beanType);

    void close();
}
