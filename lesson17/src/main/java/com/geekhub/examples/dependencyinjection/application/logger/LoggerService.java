package com.geekhub.examples.dependencyinjection.application.logger;

public interface LoggerService {
    void print(Object object);
}
