package com.geekhub.examples.dependencyinjection.application;

import com.geekhub.examples.dependencyinjection.application.printer.PrimeNumberPrinter;
import com.geekhub.examples.dependencyinjection.dependencyinjector.annotations.Bean;

import javax.inject.Inject;

@Bean
public class Worker {
    @Inject private PrimeNumberPrinter primeNumberPrinter;

    public void printPrimeNumbers(int number) {
        primeNumberPrinter.printPrimeNumbers(number);
    }
}
