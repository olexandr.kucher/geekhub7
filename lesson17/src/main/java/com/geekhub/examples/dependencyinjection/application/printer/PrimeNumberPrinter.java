package com.geekhub.examples.dependencyinjection.application.printer;

public interface PrimeNumberPrinter {
    void printPrimeNumbers(int count);
}
