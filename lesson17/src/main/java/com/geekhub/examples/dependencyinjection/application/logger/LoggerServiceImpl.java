package com.geekhub.examples.dependencyinjection.application.logger;

import com.geekhub.examples.dependencyinjection.dependencyinjector.annotations.Bean;
import com.geekhub.examples.dependencyinjection.dependencyinjector.annotations.Destroy;
import com.geekhub.examples.dependencyinjection.dependencyinjector.annotations.Init;

@Bean
public class LoggerServiceImpl implements LoggerService {
    @Override
    public void print(Object object) {
        System.out.println(object);
    }

    @Init
    public void init() {
        System.out.println("Initialized.");
    }

    @Destroy
    private void destroy() {
        System.out.println("Destroyed.");
    }
}
