package com.geekhub.examples.strategy.strategies;

import com.geekhub.examples.Exam;

public interface PassExamStrategy {
    void passExam(Exam exam);
}
