package com.geekhub.examples.strategy;

import com.geekhub.examples.strategy.strategies.PassExamStrategy;

public class Student {
    private PassExamStrategy strategy;

    public Student(PassExamStrategy strategy) {
        this.strategy = strategy;
    }

    public PassExamStrategy getStrategy() {
        return strategy;
    }
}
