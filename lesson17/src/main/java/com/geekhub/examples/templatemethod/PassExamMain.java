package com.geekhub.examples.templatemethod;

import com.geekhub.examples.Exam;
import com.geekhub.examples.templatemethod.templates.BadStudentPassExam;
import com.geekhub.examples.templatemethod.templates.ExcellentStudentPassExam;
import com.geekhub.examples.templatemethod.templates.RegularStudentPassExam;

import java.time.LocalDateTime;
import java.time.Month;

public class PassExamMain {
    public static void main(String[] args) {
        final Exam exam = new Exam();
        exam.setName("Programing Languages");
        exam.setTeacher("Teacher Name");
        exam.setTime(LocalDateTime.of(2018, Month.MAY, 25, 9, 0));

        passExam(new Student(new ExcellentStudentPassExam()), exam);
        passExam(new Student(new BadStudentPassExam()), exam);
        passExam(new Student(new RegularStudentPassExam()), exam);
    }

    private static void passExam(Student student, Exam exam) {
        student.getTemplate().passExam(exam);
    }
}
