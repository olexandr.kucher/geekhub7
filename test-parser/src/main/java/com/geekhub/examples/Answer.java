package com.geekhub.examples;

public class Answer {
    private final String id;
    private final String label;
    private final boolean isCorrect;

    public Answer(String id, String label, boolean isCorrect) {
        this.id = id;
        this.label = label;
        this.isCorrect = isCorrect;
    }

    public String getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public boolean isCorrect() {
        return isCorrect;
    }

    @Override
    public String toString() {
        return "Answer{" +
                "id='" + id + '\'' +
                ", label='" + label + '\'' +
                ", isCorrect=" + isCorrect +
                '}';
    }
}
