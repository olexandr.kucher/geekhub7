package com.geekhub.examples;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class QuestionParser {
    public static final String ANSWER_MARKER = "|";
    private final String QUESTION_ID_END_SYMBOL = ".";
    private final Predicate<String> answerCorrectnessChecker;

    public QuestionParser(Predicate<String> answerCorrectnessChecker) {
        this.answerCorrectnessChecker = answerCorrectnessChecker;
    }

    List<Question> parseQuestions(String[] questionsRawText) {
        return Arrays.stream(questionsRawText)
                .map(questionRawText -> parseQuestion(questionRawText, this.answerCorrectnessChecker))
                .collect(Collectors.toList());
    }

    Question parseQuestion(String questionRawText, Predicate<String> answerCorrectnessChecker) {
        AnswerParser answerParser = new AnswerParser();
        String[] questionLines = questionRawText
                .trim()
                .split("\\R");
        String body = Arrays.stream(questionLines)
                .filter(line -> !line.startsWith(ANSWER_MARKER))
                .collect(Collectors.joining(Main.NEW_LINE));
        List<Answer> answersText = Arrays.stream(questionLines)
                .filter(line -> line.startsWith(ANSWER_MARKER))
                .map(a -> answerParser.parseAnswer(a, answerCorrectnessChecker))
                .collect(Collectors.toList());
        String id = body.substring(0, body.indexOf(QUESTION_ID_END_SYMBOL));
        String text = body.substring(body.indexOf(QUESTION_ID_END_SYMBOL) + 1).trim();
        return new Question(Integer.valueOf(id), text, answersText);
    }
}