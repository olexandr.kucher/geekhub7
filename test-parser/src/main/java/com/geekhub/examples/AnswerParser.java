package com.geekhub.examples;

import java.util.function.Predicate;

public class AnswerParser {
    private static final String ANSWER_MARKER_END_SYMBOL = ")";

    Answer parseAnswer(String answerText, Predicate<String> correctnessChecker) {
        String id = answerText.substring(QuestionParser.ANSWER_MARKER.length(), answerText.indexOf(ANSWER_MARKER_END_SYMBOL));
        String label = answerText.substring(answerText.indexOf(ANSWER_MARKER_END_SYMBOL) + 1).trim();
        boolean correct = correctnessChecker.test(id);

        return new Answer(id, label, correct);
    }
}