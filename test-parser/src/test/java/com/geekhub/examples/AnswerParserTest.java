package com.geekhub.examples;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AnswerParserTest {

    private AnswerParser answerParser;

    @Before
    public void setUp() throws Exception {
        answerParser = new AnswerParser();
    }

    @Test
    public void parseAnswer_shouldNormallyParseId_forCorrectInput(){

        String answerRawText = "|a) ExecutorService es = ExecutorService.getInstance();";
        Answer answer = answerParser.parseAnswer(answerRawText, id -> id.startsWith("_"));
        Assert.assertEquals("a", answer.getId());
    }


    @Test
    public void parseAnswer_shouldNormallyParseLabel_forCorrectInput(){
        String answerRawText = "|a) ExecutorService es = ExecutorService.getInstance();";
        Answer answer = answerParser.parseAnswer(answerRawText, id -> id.startsWith("_"));
        Assert.assertEquals("ExecutorService es = ExecutorService.getInstance();", answer.getLabel());
    }


    @Test
    public void parseAnswer_shouldNormallyParseCorrectness_forInCorrectAnswer(){
        String answerRawText = "|a) ExecutorService es = ExecutorService.getInstance();";
        Answer answer = answerParser.parseAnswer(answerRawText, id -> id.startsWith("_"));
        Assert.assertEquals(false, answer.isCorrect());
    }


    @Test
    public void parseAnswer_shouldNormallyParseCorrectness_forCorrectAnswer(){
        String answerRawText = "|_) ExecutorService es = ExecutorService.getInstance();";
        Answer answer = answerParser.parseAnswer(answerRawText, id -> id.startsWith("_"));
        Assert.assertEquals(true, answer.isCorrect());
    }

}