package com.geekhub.lessons;

import java.util.Collection;

public interface UserSource {
    Collection<User> getUsers();

    void addUser(User user);

}
