package com.geekhub.lessons.transport;

import com.geekhub.lessons.interfaces.Drivable;
import com.geekhub.lessons.parts.Accelerator;
import com.geekhub.lessons.parts.Engine;
import com.geekhub.lessons.parts.FrontWheel;
import com.geekhub.lessons.parts.RearWheel;

public class Motorcycle implements Drivable {
    @Override
    public Accelerator getAccelerator() {
        return null;
    }

    @Override
    public Engine getEngine() {
        return null;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public byte getNumberOfWheels() {
        return 0;
    }

    @Override
    public FrontWheel[] getFrontWheels() {
        return new FrontWheel[0];
    }

    @Override
    public RearWheel[] getRearWheels() {
        return new RearWheel[0];
    }
}
