package com.geekhub.lessons.transport;

import com.geekhub.lessons.parts.RearWheel;
import com.geekhub.lessons.DriveType;
import com.geekhub.lessons.parts.FrontWheel;
import com.geekhub.lessons.parts.Accelerator;
import com.geekhub.lessons.parts.Engine;

public class Coupe extends Vehicle {
    public Coupe(DriveType driveType, String name, Accelerator accelerator, Engine engine, FrontWheel[] front, RearWheel[] rear) {
        super(2, driveType, name, accelerator, engine, front, rear);
    }
}
