package com.geekhub.lessons.transport;

import com.geekhub.lessons.interfaces.Drivable;
import com.geekhub.lessons.DriveType;
import com.geekhub.lessons.parts.Accelerator;
import com.geekhub.lessons.parts.Engine;
import com.geekhub.lessons.parts.FrontWheel;
import com.geekhub.lessons.parts.RearWheel;

public abstract class Vehicle implements Drivable {
    protected final int numberOfDoors;
    protected final DriveType driveType;
    protected int weight;
    protected final byte numberOfWheels;
    protected final String name;
    protected final Accelerator accelerator;
    protected final Engine engine;
    protected final FrontWheel[] front;
    protected final RearWheel[] rear;

    protected Vehicle(int numberOfDoors, DriveType driveType,
                      String name, Accelerator accelerator, Engine engine,
                      FrontWheel[] front, RearWheel[] rear) {
        this.numberOfDoors = numberOfDoors;
        this.driveType = driveType;
        this.name = name;
        this.accelerator = accelerator;
        this.engine = engine;
        this.front = front;
        this.rear = rear;
        this.numberOfWheels = 4;
    }

    @Override
    public byte getNumberOfWheels() {
        return numberOfWheels;
    }

    @Override
    public Accelerator getAccelerator() {
        return accelerator;
    }

    @Override
    public Engine getEngine() {
        return engine;
    }

    public int getNumberOfDoors() {
        return numberOfDoors;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public DriveType getDriveType() {
        return driveType;
    }


    @Override
    public String getName() {
        return name;
    }

    @Override
    public FrontWheel[] getFrontWheels() {
        return front;
    }

    @Override
    public RearWheel[] getRearWheels() {
        return rear;
    }
}
