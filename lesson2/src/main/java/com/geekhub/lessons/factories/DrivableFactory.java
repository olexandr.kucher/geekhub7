package com.geekhub.lessons.factories;

import com.geekhub.lessons.interfaces.Drivable;

public interface DrivableFactory {
    Drivable create();
}
