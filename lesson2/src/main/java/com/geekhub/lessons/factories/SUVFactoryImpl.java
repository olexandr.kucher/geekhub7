package com.geekhub.lessons.factories;

import com.geekhub.lessons.DriveType;
import com.geekhub.lessons.parts.RearWheel;
import com.geekhub.lessons.transport.SUV;
import com.geekhub.lessons.parts.FrontWheel;
import com.geekhub.lessons.parts.Accelerator;
import com.geekhub.lessons.parts.Engine;
import com.geekhub.lessons.parts.EngineType;

public class SUVFactoryImpl implements DrivableFactory {
    @Override
    public SUV create() {
        return new SUV(
                DriveType.FULL_DRIVE, "RAW4",
                new Accelerator(), new Engine(150, EngineType.DIESEL),
                new FrontWheel[] {new FrontWheel(), new FrontWheel()},
                new RearWheel[] {new RearWheel(), new RearWheel()}
        );
    }
}
