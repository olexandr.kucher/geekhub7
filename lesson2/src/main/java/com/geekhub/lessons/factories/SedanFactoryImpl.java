package com.geekhub.lessons.factories;

import com.geekhub.lessons.parts.RearWheel;
import com.geekhub.lessons.DriveType;
import com.geekhub.lessons.parts.FrontWheel;
import com.geekhub.lessons.transport.Sedan;
import com.geekhub.lessons.parts.Accelerator;
import com.geekhub.lessons.parts.Engine;
import com.geekhub.lessons.parts.EngineType;

public class SedanFactoryImpl implements DrivableFactory {
    @Override
    public Sedan create() {
        return new Sedan(
                DriveType.FRONT_DIVE, "Lancer X",
                new Accelerator(), new Engine(150, EngineType.GAS),
                new FrontWheel[] {new FrontWheel(), new FrontWheel()},
                new RearWheel[] {new RearWheel(), new RearWheel()}
        );
    }
}
