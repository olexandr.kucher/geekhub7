package com.geekhub.lessons.interfaces;

import com.geekhub.lessons.parts.Accelerator;
import com.geekhub.lessons.parts.Engine;
import com.geekhub.lessons.parts.FrontWheel;
import com.geekhub.lessons.parts.RearWheel;

public interface Drivable {
    Accelerator getAccelerator();

    Engine getEngine();

    String getName();

    byte getNumberOfWheels();

    FrontWheel[] getFrontWheels();

    RearWheel[] getRearWheels();

    default Steered[] getSteeringWheels() {
        return getFrontWheels();
    }
}
