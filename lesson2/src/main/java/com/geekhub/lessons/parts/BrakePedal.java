package com.geekhub.lessons.parts;

import com.geekhub.lessons.interfaces.StatusAware;

public class BrakePedal implements StatusAware {
    private String status;

    @Override
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return status;
    }
}
