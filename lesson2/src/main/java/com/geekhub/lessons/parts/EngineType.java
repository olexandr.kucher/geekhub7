package com.geekhub.lessons.parts;

public enum EngineType {
    DIESEL, GAS, ELECTRIC
}
