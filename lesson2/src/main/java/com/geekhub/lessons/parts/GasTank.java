package com.geekhub.lessons.parts;

import com.geekhub.lessons.interfaces.StatusAware;

public class GasTank implements StatusAware {
    private final int maxVolume;
    private int currentVolume;

    public GasTank(int maxVolume) {
        this.maxVolume = maxVolume;
    }

    @Override
    public String getStatus() {
        return String.valueOf(currentVolume);
    }

    public void setCurrentVolume(int currentVolume) {
        if (currentVolume < 0 || currentVolume > maxVolume) {
            throw new IllegalArgumentException("Wrong volume specified: " + currentVolume);
        }
        this.currentVolume = currentVolume;
    }

    public int getMaxVolume() {
        return maxVolume;
    }

    @Override
    public String toString() {
        return currentVolume + " of " + maxVolume;
    }
}
