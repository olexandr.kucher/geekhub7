package com.geekhub.lessons.parts;

import com.geekhub.lessons.interfaces.Retarding;
import com.geekhub.lessons.interfaces.Steered;

public class FrontWheel implements Steered, Retarding {
    @Override
    public void slowDown() {
        System.out.println("STOP");
    }

    @Override
    public void turnLeft() {
        System.out.println("LEFT");
    }

    @Override
    public void turnRight() {
        System.out.println("RIGHT");
    }
}
