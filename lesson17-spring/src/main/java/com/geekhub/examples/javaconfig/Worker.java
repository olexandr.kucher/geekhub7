package com.geekhub.examples.javaconfig;

import com.geekhub.examples.javaconfig.printer.PrimeNumberPrinter;

public class Worker {
    private PrimeNumberPrinter primeNumberPrinter;

    public Worker(PrimeNumberPrinter primeNumberPrinter) {
        this.primeNumberPrinter = primeNumberPrinter;
    }

    public void printPrimeNumbers(int number) {
        primeNumberPrinter.printPrimeNumbers(number);
    }
}
