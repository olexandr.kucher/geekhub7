package com.geekhub.examples.javaconfig.logger;

public interface LoggerService {
    void print(Object object);
}
