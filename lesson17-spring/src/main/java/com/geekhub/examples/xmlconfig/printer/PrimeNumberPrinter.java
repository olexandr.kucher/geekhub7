package com.geekhub.examples.xmlconfig.printer;

public interface PrimeNumberPrinter {
    void printPrimeNumbers(int count);
}
