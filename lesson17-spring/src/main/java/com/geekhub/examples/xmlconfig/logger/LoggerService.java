package com.geekhub.examples.xmlconfig.logger;

public interface LoggerService {
    void print(Object object);
}
