package com.geekhub.examples.annotationconfig.logger;

public interface LoggerService {
    void print(Object object);
}
