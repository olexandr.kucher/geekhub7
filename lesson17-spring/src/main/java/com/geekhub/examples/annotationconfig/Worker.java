package com.geekhub.examples.annotationconfig;

import com.geekhub.examples.annotationconfig.printer.PrimeNumberPrinter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Worker {
    private final PrimeNumberPrinter primeNumberPrinter;

    @Autowired
    public Worker(PrimeNumberPrinter primeNumberPrinter) {
        this.primeNumberPrinter = primeNumberPrinter;
    }

    public void printPrimeNumbers(int number) {
        primeNumberPrinter.printPrimeNumbers(number);
    }
}
