package com.geekhub.examples;

import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.TaskAction;

public class MyFirstTask extends DefaultTask {
    public MyFirstTask() {
        setDescription("My first Gradle task on Java. Task makes testString.toUpperCase().");
    }

    private String testString;

    @TaskAction
    public void action() {
        System.out.println("Task: " + getName() + ", description: " + getDescription());
        System.out.println("This is action of my first gradle task.");
        if (null != testString) {
            System.out.println("Original string: " + testString + ", uppercase string: " + testString.toUpperCase());
        } else {
            System.out.println("You didn't use customization for testString.");
        }
    }

    public String getTestString() {
        return testString;
    }

    public void setTestString(String testString) {
        this.testString = testString;
    }
}
