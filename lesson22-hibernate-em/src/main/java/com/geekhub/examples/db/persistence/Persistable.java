package com.geekhub.examples.db.persistence;

import java.io.Serializable;

public interface Persistable<PK extends Serializable> {
    PK getId();
}
