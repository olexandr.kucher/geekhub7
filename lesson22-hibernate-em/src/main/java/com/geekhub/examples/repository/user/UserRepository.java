package com.geekhub.examples.repository.user;

import com.geekhub.examples.db.persistence.User;
import com.geekhub.examples.repository.GeneralRepository;

import java.util.Optional;

public interface UserRepository extends GeneralRepository<User, Integer> {
    Optional<User> findBy(String username);
}
