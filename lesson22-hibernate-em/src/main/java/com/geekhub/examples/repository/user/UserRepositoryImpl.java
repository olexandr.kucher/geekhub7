package com.geekhub.examples.repository.user;

import com.geekhub.examples.db.persistence.User;
import com.geekhub.examples.repository.GeneralRepositoryImpl;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class UserRepositoryImpl extends GeneralRepositoryImpl<User, Integer> implements UserRepository {
    public UserRepositoryImpl() {
        super(User.class);
    }

    @Override
    public Optional<User> findBy(String username) {
        return entityManager.createQuery("SELECT u FROM user u WHERE u.username = :username", User.class)
                .setParameter("username", username)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst();
    }
}
