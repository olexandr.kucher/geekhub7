package com.geekhub.examples.service;

import com.geekhub.examples.db.persistence.License;
import com.geekhub.examples.db.persistence.User;

public interface UserLicenseService {
    void save(User user, License license);

    void delete(User user);
}
