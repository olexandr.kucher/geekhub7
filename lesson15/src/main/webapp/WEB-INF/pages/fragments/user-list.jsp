<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:forEach var="user" items="${users}">
    <br>Username: <c:out value="${user}"/>
    <c:choose>
        <c:when test="${user == 'Tommy'}">
            need Jerry
        </c:when>
        <c:when test="${user == 'Rikki'}">
            need Tikki
        </c:when>
        <c:otherwise>
            <c:out value="<script >alert('OOPS, hacked')</script>"/>
        </c:otherwise>
    </c:choose>
</c:forEach>
