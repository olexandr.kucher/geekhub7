package io.geekhub.tests;

public interface OperationWithOneArgument extends Operation {

   Number calculate(Number argument);

}
