package io.geekhub.tests;

import org.mockito.Mockito;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.testng.Assert.*;

public class CalculatorWithOperationsTest {
    CalculatorWithOperations calculator;

    private OperationWithOneArgument sqrtOperation;
    private OperationWithTwoArguments sumOperation;

    @BeforeMethod
    public void setUp() throws Exception {
        sqrtOperation = Mockito.mock(OperationWithOneArgument.class);
        Mockito.when(sqrtOperation.calculate(any(Number.class))).thenAnswer(invocation -> {
            return Math.sqrt(invocation.getArgument(0));
        });

        calculator = Mockito.spy(
                new CalculatorWithOperations(sqrtOperation, (n1 ,n2 ) -> (n1.doubleValue()*n2.doubleValue()))
        );

        doReturn(6.0).when(calculator).add(4.0, 2.0);
    }

    @Test
    public void testName() throws Exception {
        assertEquals(calculator.add(4.0, 2.0), 6.0);
    }

    @Test
    public void testName1() throws Exception {
        assertEquals(calculator.sqrt(9), 3.0);
    }
}