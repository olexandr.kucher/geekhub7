package com.geekhub.examples.services.bookmanager;

import com.geekhub.examples.data.dto.Book;

public interface BookManager {
    void putBook(Book book);

    Book getBook(long isbn);
}
