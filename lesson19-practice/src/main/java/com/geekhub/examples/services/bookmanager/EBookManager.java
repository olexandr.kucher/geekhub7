package com.geekhub.examples.services.bookmanager;

import com.geekhub.examples.data.dto.Book;
import com.geekhub.examples.services.bookcontainer.BookContainer;
import com.geekhub.examples.services.logger.LoggerService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class EBookManager implements BookManager {
    private final BookContainer bookContainer;
    private final LoggerService loggerService;

    public EBookManager(@Qualifier("EBookStorage") BookContainer bookContainer, LoggerService loggerService) {
        this.bookContainer = bookContainer;
        this.loggerService = loggerService;
    }

    @Override
    public void putBook(Book book) {
        bookContainer.putBook(book);
    }

    @Override
    public Book getBook(long isbn) {
        return bookContainer.getBook(isbn);
    }
}
