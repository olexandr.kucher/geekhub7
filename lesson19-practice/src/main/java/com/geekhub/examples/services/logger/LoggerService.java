package com.geekhub.examples.services.logger;

public interface LoggerService {
    void log(Object object);
}
