package com.geekhub.examples.mvcpattern.views;

public interface View<T> {
    void show(T data);
}
