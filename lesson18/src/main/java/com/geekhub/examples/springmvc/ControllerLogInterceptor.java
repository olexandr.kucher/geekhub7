package com.geekhub.examples.springmvc;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;

public class ControllerLogInterceptor extends HandlerInterceptorAdapter {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        if (handler instanceof HandlerMethod) {
            System.out.println("[" + LocalDateTime.now() + "]: ["
                               + ((HandlerMethod) handler).getBeanType() + "]   -   START:    "
                               + request.getMethod() + " " + request.getRequestURI());
        }
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        if (handler instanceof HandlerMethod) {
            if (ex != null) {
                System.out.println("[" + LocalDateTime.now() + "]: ["
                                   + ((HandlerMethod) handler).getBeanType() + "]   -   ERROR:    "
                                   + request.getMethod() + " " + request.getRequestURI() + ": " + ex.toString());
                response.sendRedirect("error.html");
            } else {
                System.out.println("[" + LocalDateTime.now() + "]: ["
                                   + ((HandlerMethod) handler).getBeanType() + "]   -   COMPLETE: "
                                   + request.getMethod() + " " + request.getRequestURI());
            }
        }
    }
}
