package com.geekhub.examples.repository;

import com.geekhub.examples.db.persistence.User;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Optional<User> findBy(Integer id) {
        if (null == id) {
            return Optional.empty();
        } else {
            return Optional.ofNullable(sessionFactory.getCurrentSession().get(User.class, id));
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public Optional<User> findBy(String username) {
        return sessionFactory.getCurrentSession()
                .createCriteria(User.class)
                .add(Restrictions.eq("username", username))
                .setMaxResults(1)
                .list()
                .stream()
                .findFirst();
    }

    @Override
    public List<User> findAll() {
        return sessionFactory.getCurrentSession().createCriteria(User.class).list();
    }

    @Override
    public User save(User user) {
        sessionFactory.getCurrentSession().save(user);
        return user;
    }

    @Override
    public void delete(User user) {
        sessionFactory.getCurrentSession().delete(user);
    }
}
