package com.geekhub.examples.repository;

import com.geekhub.examples.db.persistence.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {
    Optional<User> findBy(Integer id);

    Optional<User> findBy(String username);

    List<User> findAll();

    User save(User user);

    void delete(User user);
}
